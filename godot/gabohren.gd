extends Node2D

const MOD_TYPE_LINEAR = 1
const MOD_TYPE_EXPONENTIAL = 2
const MOD_TYPE_SOUND = 3

const PARAM_NAMES = ["scale", "frequency", "speed_rotation",
					 "speed_phase", "offset_rotation", "offset_phase",
					 "contrast:RG", "contrast:BY", "contrast:BW", "sound:volume",
					 "sound:distance"]
const PARAM_DATA = {"scale":
	{"name": "Scale",
	 "blurb": "All the surrounding Gabor patches are the same size, " +
			  "except one (which could be larger or smaller). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for size. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.5,
	 "modMax": 1.0,
	 "modMin": -2.0,
	 "modDiff": 1.5,
	 "modType": MOD_TYPE_EXPONENTIAL},
	"frequency":
	{"name": "Frequency",
	 "blurb": "All the surrounding Gabor patches have the same number of, " +
			  "waves except one (which could have more or less). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for frequency. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMax": 5.5,
	 "modMin": 0.5,
	 "modDiff": 2.5,
	 "modType": MOD_TYPE_LINEAR},
	"contrast:BW":
	{"name": "Black/White Contrast",
	 "blurb": "All the surrounding Gabor patches have the same black/white separation, " +
			  "except one (which could have more extreme or less extreme separation). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for black/white contrast. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.25,
	 "modMax": 2.0,
	 "modMin": -2.0,
	 "modDiff": 2.0,
	 "modType": MOD_TYPE_EXPONENTIAL},
	"contrast:RG":
	{"name": "Red/Green Contrast",
	 "blurb": "All the surrounding Gabor patches have the same red/green separation, " +
			  "except one (which could have more extreme or less extreme separation). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for red/green contrast. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.25,
	 "modMax": 2.0,
	 "modMin": -2.0,
	 "modDiff": 2.0,
	 "modType": MOD_TYPE_EXPONENTIAL},
	"contrast:BY":
	{"name": "Blue/Yellow Contrast",
	 "blurb": "All the surrounding Gabor patches have the same blue/yellow separation, " +
			  "except one (which could have more or less extreme separation). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for blue/yellow contrast. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.25,
	 "modMax": 2.0,
	 "modMin": -2.0,
	 "modDiff": 2.0,
	 "modType": MOD_TYPE_EXPONENTIAL},
	"speed_rotation":
	{"name": "Rotation Speed",
	 "blurb": "All the surrounding Gabor patches are rotating at the same, " +
			  "speed except one (which could be faster or slower). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for rotation speed. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMax": 2.0,
	 "modMin": -2.0,
	 "modDiff": 2.0,
	 "modType": MOD_TYPE_LINEAR},
	"speed_phase":
	{"name": "Phase Speed",
	 "blurb": "All the surrounding Gabor patches are cycling at the same, " +
			  "speed except one (which could be cycling faster or slower). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for cycling speed. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMax": 2.0,
	 "modMin": -2.0,
	 "modDiff": 2.0,
	 "modType": MOD_TYPE_LINEAR},
	"offset_rotation":
	{"name": "Rotation Offset",
	 "blurb": "All the surrounding Gabor patches are aligned in the same way " +
			  "relative to their radial axis except one (which could be " +
			  "rotated clockwise or counter-clockwise). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for rotation offset. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMin": -90.0,
	 "modMax": 90.0,
	 "modDiff": 90.0,
	 "modType": MOD_TYPE_LINEAR},
	"offset_phase":
	{"name": "Phase Offset",
	 "blurb": "All the surrounding Gabor patches have the same wave phase " +
			  "except one (which could be shifted inwards or outwards). Click " +
			  "on the different patch within 20 seconds to advance the " +
			  "skill level for phase offset. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMin": -0.5,
	 "modMax": 0.5,
	 "modDiff": 0.5,
	 "modType": MOD_TYPE_LINEAR},
	"sound:distance":
	{"name": "Sound Distance",
	 "blurb": "The surrounding Gabor patches all look identical. Hovering over " +
			  "a patch will play the sound associated with that patch. Periodically, " +
			  "a sound will be played that matches the sound from one of the patches, " +
			  "getting closer to the centre as skill level increases. " +
			  "Click on the noisy patch within 20 seconds to advance the " +
			  "skill level for sound distance. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMin": 0.0,
	 "modMax": 1.0,
	 "modDiff": 0.5,
	 "modType": MOD_TYPE_SOUND},
	"sound:volume":
	{"name": "Sound Volume",
	 "blurb": "The surrounding Gabor patches all look identical. Hovering over " +
			  "a patch will play the sound associated with that patch. Periodically, " +
			  "a sound will be played that matches the sound from one of the patches, " +
			  "getting quieter as skill level increases. " +
			  "Click on the similar patch within 20 seconds to advance the " +
			  "skill level for volume. The skill level is reduced when " +
			  "the wrong patch is clicked on, or more than 20 seconds " +
			  "has passed.\n\n" +
			  "After two failures in a row, the skill level will be " +
			  "locked in and set to the last successful level.",
	 "base": 0.0,
	 "modMin": 0.0,
	 "modMax": 1.0,
	 "modDiff": 0.5,
	 "modType": MOD_TYPE_SOUND},
	}

var lastWindowMode = DisplayServer.WINDOW_MODE_WINDOWED
var current_scene = null
var selectedTrainer = ""
var activeScene = ""
var keyCoolDown = 0
var skillLevels: Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() - 1)
	## Load old skill information
	## https://docs.godotengine.org/en/stable/tutorials/io/saving_games.html
	if FileAccess.file_exists("user://skills.save"):
		var save_file = FileAccess.open("user://skills.save", FileAccess.READ)
		while save_file.get_position() < save_file.get_length():
			var json_string = save_file.get_line()
			var parse_result = JSON.parse_string(json_string)
			if(parse_result is Dictionary):
				for key in parse_result.keys():
					skillLevels[key] = parse_result[key]

## https://docs.godotengine.org/en/stable/tutorials/scripting/singletons_autoload.html
func goto_scene(path):
	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path):
	current_scene.free()
	var s = ResourceLoader.load(path)
	current_scene = s.instantiate()
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if((keyCoolDown <= 0) and Input.is_action_pressed("ui_cancel")):
		keyCoolDown = 2.0
		if(activeScene == "Gabohren"):
			get_tree().quit()
		else:
			goto_scene("res://gabohren.tscn")
	if(Input.is_action_just_released("ui_cancel")):
		keyCoolDown = 0.0
	if(Input.is_action_just_pressed("input_fullScreen")):
		if(DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_FULLSCREEN):
			DisplayServer.window_set_mode(lastWindowMode)
		else:
			lastWindowMode = DisplayServer.window_get_mode()
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	if(keyCoolDown > 0):
		keyCoolDown -= delta
