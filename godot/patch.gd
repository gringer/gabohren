extends Area2D

@onready var patchCover: AnimatedSprite2D = $Cover
@onready var patch: Area2D = self
@onready var selectedLabel: Label = $Label_Selected
@onready var root: Node = self.get_tree().root.get_node("Gabohren")
@onready var levelLabel: Label = self.get_parent().get_node("Label_Level")

# Called when the node enters the scene tree for the first time.
func _ready():
	root.activeScene = "Gabohren"
	patchCover.frame = 0
	selectedLabel.visible = false
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_mouse_shape_entered(shape_idx):
	var patchShape: CollisionShape2D = patch.get_child(shape_idx)
	patchCover.visible = true
	patchCover.transform.origin = patchShape.transform.origin
	selectedLabel.text = patchShape.get_meta("label")
	if((patchShape.name == "Patch_Challenge") || (patchShape.name == "Patch_Random")):
		levelLabel.text = ""
	else:
		if(patchShape.get_meta("paramName") and (patchShape.get_meta("paramName") in root.skillLevels)):
			levelLabel.text = "Current level for %s\n%d" % \
				[patchShape.get_meta("label"), root.skillLevels[patchShape.get_meta("paramName")]]
		else:
			levelLabel.text = "Current level for %s\n0" % \
				[patchShape.get_meta("label")]
	selectedLabel.position = patchShape.position + Vector2(-selectedLabel.size.x/2.0, 120)
	selectedLabel.visible = true


func _on_mouse_shape_exited(_shape_idx):
	patchCover.visible = false
	selectedLabel.visible = false
	patchCover.frame = 0

func _on_input_event(_viewport, event, shape_idx):
	var patchShape: CollisionShape2D = patch.get_child(shape_idx)
	if event is InputEventMouseButton:
		if(event.pressed):
			if(patchShape.name == "Patch_Challenge"):
				patchCover.frame = 1
				root.selectedTrainer = ""
				root.goto_scene("res://challenge.tscn")
			else:
				patchCover.frame = 2
				root.selectedTrainer = patchShape.get_meta("paramName")
				root.goto_scene("res://trainer.tscn")
		else:
			patchCover.frame = 0
	pass # Replace with function body.
