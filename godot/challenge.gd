extends Area2D

@onready var patchCover: AnimatedSprite2D = $Cover
@onready var surroundFail: Sprite2D = self.get_parent().get_node("SurroundFail")
@onready var patch: Area2D = self
@onready var timeLabel: Label = $TimeLabel
@onready var featureLabel: Label = $FeatureLabel
@onready var scoreLabel: Label = $ScoreLabel
@onready var gameNode: Node2D = get_parent()
@onready var root: Node = self.get_tree().root.get_node("Gabohren")
@onready var blurbText: RichTextLabel = self.get_parent().get_node("BlurbText")
@onready var soundListener: AudioListener2D = self.get_parent().get_node("Listener_Primary")
@onready var soundNote: AudioStreamPlayer2D
@onready var differenceCount = root.PARAM_NAMES.size()

const PATCH_NAMES = ["Patch_N","Patch_NE","Patch_E","Patch_SE",
					 "Patch_S","Patch_SW","Patch_W","Patch_NW"]
const TIME_MAX = 20.0
const TIMEOUT_PATCH_ALL = 0.2
const TIMEOUT_PATCH_SELECT = 0.2
const SOUND_DELAY_START = 5.0
const MOD_TYPE_LINEAR = 1
const MOD_TYPE_EXPONENTIAL = 2
const MOD_TYPE_SOUND = 3

var modifiedPatch = PATCH_NAMES[0]
var points = 0
var successCount = 0
var failCount = 0
var minScoreLevel = 0

var runTime = 0.0
var patchAllTimeout = 0.0
var patchSelectTimeout = 0.0
var soundTimeout = 0.0
var soundDelay = SOUND_DELAY_START
var paramsToSet: Array

func finishTrainer():
	var save_file = FileAccess.open("user://skills.save", FileAccess.WRITE)
	save_file.store_line(JSON.stringify(root.skillLevels))
	if(root.selectedTrainer != ""):
		root.goto_scene("res://gabohren.tscn")
	else:
		resetShapes()

# Called when the node enters the scene tree for the first time.
func _ready():
	root.activeScene = "Trainer"
	randomize()
	resetShapes()
	soundListener.make_current()
	blurbText.visible = true

func getAngleColour(theta: float):
	## top: red, right: yellow, bottom: [dark] green, left: blue
	theta = fmod(theta, 360)
	var h
	var s = 1
	var v = 1
	if ((theta >=   0.0) && (theta <  90.0)):
		h = (theta * (60.0 / 90.0))
	elif ((theta >=  90.0) && (theta < 180.0)):
		h = ((theta - 90.0) * (60.0 / 90.0)) + 60.0
		v = 1 - ((theta - 90.0) / 180.0)
	elif ((theta >= 180.0) && (theta < 270.0)):
		h = ((theta - 180.0) * (120.0 / 90.0)) + 120.0
		v = ((theta - 180.0) / 180.0) + 0.5
	elif ((theta >= 270.0) && (theta < 360.0)):
		h = ((theta - 270.0) * (120.0 / 90.0)) + 240.0
	## hsvToRgb
	var retCol: Color
	var i
	if (s == 0):
		retCol = Color(v, v, v)
		return(retCol)
	h = h / 60
	i = floor(h)
	var data = [v*(1-s), v*(1-s*(h-i)), v*(1-s*(1-(h-i)))]
	if(i == 0):
		retCol = Color(v, data[2], data[0])
	elif(i == 1):
		retCol = Color(data[1], v, data[0])
	elif(i == 2):
		retCol = Color(data[0], v, data[2])
	elif(i == 3):
		retCol = Color(data[0], data[1], v)
	elif(i == 4):
		retCol = Color(data[2], data[0], v)
	else:
		retCol = Color(v, data[0], data[1])
	return(retCol)

func resetShapes():
	var epsilon = 0.001
	modifiedPatch = PATCH_NAMES[randi() % PATCH_NAMES.size()]
	soundTimeout = 0
	paramsToSet = root.PARAM_NAMES.duplicate()
	paramsToSet.shuffle()
	paramsToSet.resize(differenceCount)
	featureLabel.text = "Feature level: " + str(root.PARAM_NAMES.size() - differenceCount + 1) + \
		" / " + str(successCount + 1)
	var redGreenLevel = (root.skillLevels["contrast:RG"] if "contrast:RG" in root.skillLevels else 0) + 1
	var blueYellowLevel = (root.skillLevels["contrast:BY"] if "contrast:BY" in root.skillLevels else 0) + 1
	# pick a random clock colour angle, and choose opposites for the colours
	var angleFwd = randf() * 360
	var angleColFwd = getAngleColour(angleFwd)
	var angleColComp = getAngleColour(angleFwd + 180)
	# change colours based on RG/BY
	var rgProp = sin(angleFwd) * 1.0 / (1.2 ** redGreenLevel)
	var byProp = cos(angleFwd) * 1.0 / (1.2 ** blueYellowLevel)
	for iChild in self.get_children():
		if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
			var icImage: Sprite2D = iChild.get_node("PatchImage")
			var icMaterial := icImage.material as ShaderMaterial
			## Reset everything to default values
			icMaterial.set_shader_parameter("speed_phase", 0.0)
			icMaterial.set_shader_parameter("speed_rotation", 0.0)
			icMaterial.set_shader_parameter("offset_phase", 0.0)
			icMaterial.set_shader_parameter("offset_rotation", 0.0)
			icMaterial.set_shader_parameter("frequency", 2.0)
			icMaterial.set_shader_parameter("scale", 1.0)
			icMaterial.set_shader_parameter("contrast", 1.0)
			icMaterial.set_shader_parameter("colour1", angleColFwd)
			icMaterial.set_shader_parameter("colour2", angleColComp)
	minScoreLevel = 0
	# set defaults for all parameters
	for setParam in root.PARAM_NAMES:
		if((setParam == "contrast:RG") || (setParam == "contrast:BY")):
			continue
		var skillLevel = root.skillLevels[setParam] if setParam in root.skillLevels else 0
		skillLevel += 1
		if(skillLevel >= minScoreLevel):
			minScoreLevel = skillLevel
		var cParam = root.PARAM_DATA[setParam]
		if(setParam.find(":") > -1):
			setParam = setParam.substr(0, setParam.find(":"))
		var paramModDiff = (cParam["modMax"] - cParam["modMin"]) / (2.0 * 1.2 ** skillLevel)
		var paramMidLow = cParam["modMin"] + paramModDiff
		var paramMidHigh = cParam["modMax"] - paramModDiff
		var paramModMid = paramMidLow
		if(paramMidHigh - paramMidLow > epsilon):
			paramModMid = randf() * (paramMidHigh - paramMidLow) + paramMidLow
		for iChild in self.get_children():
			if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
				var icImage: Sprite2D = iChild.get_node("PatchImage")
				var icMaterial := icImage.material as ShaderMaterial
				if(cParam["modType"] == root.MOD_TYPE_EXPONENTIAL):
					icMaterial.set_shader_parameter(setParam, cParam["base"] * (2.0 ** paramModMid) )
				elif(cParam["modType"] == root.MOD_TYPE_LINEAR):
					if(setParam == "contrast:BW"):
						icMaterial.set_shader_parameter(setParam, paramModMid * rgProp * byProp )
					else:
						icMaterial.set_shader_parameter(setParam, paramModMid )
	# set variant parameters
	for setParam in paramsToSet:
		var diffDir = (randi() % 2 - 0.5) * 2.0
		if((setParam == "contrast:RG") || (setParam == "contrast:BY")):
			continue
		var skillLevel = root.skillLevels[setParam] if setParam in root.skillLevels else 0
		skillLevel += 1
		var cParam = root.PARAM_DATA[setParam]
		if(setParam.find(":") > -1):
			setParam = setParam.substr(0, setParam.find(":"))
		var paramModDiff = (cParam["modMax"] - cParam["modMin"]) / (2.0 * 1.2 ** skillLevel)
		var paramMidLow = cParam["modMin"] + paramModDiff
		var paramMidHigh = cParam["modMax"] - paramModDiff
		var paramModMid = paramMidLow
		if(paramMidHigh - paramMidLow > epsilon):
			paramModMid = randf() * (paramMidHigh - paramMidLow) + paramMidLow
		for iChild in self.get_children():
			if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
				var icImage: Sprite2D = iChild.get_node("PatchImage")
				var icMaterial := icImage.material as ShaderMaterial
				if(iChild.name == modifiedPatch):
					if(cParam["modType"] == root.MOD_TYPE_SOUND):
						pass
					elif(cParam["modType"] == root.MOD_TYPE_EXPONENTIAL):
						icMaterial.set_shader_parameter(setParam,
							cParam["base"] * (2.0 ** (paramModMid + paramModDiff * diffDir)) )
					elif(cParam["modType"] == root.MOD_TYPE_LINEAR):
						if(setParam == "contrast:BW"):
							icMaterial.set_shader_parameter(setParam,
								paramModMid + paramModDiff * diffDir * rgProp * byProp )
						else:
							icMaterial.set_shader_parameter(setParam,
								paramModMid + paramModDiff * diffDir)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	runTime += delta
	var ramp = sqrt(minf(runTime, TIME_MAX) / TIME_MAX)
	var rtScale = ramp * 1.9 + 0.1
	for iChild in self.get_children():
		if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
			var icImage: Sprite2D = iChild.get_node("PatchImage")
			icImage.scale = Vector2(rtScale, rtScale)
	if(runTime >= TIME_MAX):
		surroundFail.visible = true
		surroundFail.frame = 1
		patchAllTimeout = TIMEOUT_PATCH_ALL
		successCount = 0
		failCount += 1
		runTime = 0.0
		blurbText.visible = true
		if(failCount >= 2):
			finishTrainer()
	if(patchAllTimeout > 0):
		patchAllTimeout -= delta
	else:
		surroundFail.visible = false
	if(patchSelectTimeout > 0):
		patchSelectTimeout -= delta
		if(patchSelectTimeout <= 0):
			patchCover.visible = false
			patchSelectTimeout = 0
	if(soundTimeout <= 0):
		var patchObject = patch.get_node(modifiedPatch)
		var soundNote1 = soundListener.get_node("Note_A220_b1")
		var soundNote2 = soundListener.get_node("Note_A220_b2")
		var skillSndPos = root.skillLevels["sound:position"] if "sound:position" in root.skillLevels else 0
		soundNote1.position = patchObject.position * 1.0 / ((skillSndPos * 1.5) + 1.0)
		soundNote2.position = patchObject.position * 1.0 / ((skillSndPos * 1.5) + 1.0)
		var skillSndVol = root.skillLevels["sound:volume"] if "sound:volume" in root.skillLevels else 0
		soundNote1.attenuation = 5.0 * 2.0 ** (skillSndVol / 10.0)
		soundNote2.attenuation = 5.0 * 2.0 ** (skillSndVol / 10.0)
		soundNote1.volume_db = -skillSndVol * (1.0 - (runTime / TIME_MAX))
		var pitchCalc1 = 220 * 2.0 ** (-soundNote1.position.x / 320.0)
		var pitchCalc2 = 220 * (2.0/3.0) * 2.0 ** (soundNote2.position.y / 320.0)
		soundNote1.set_pitch_scale(220 / pitchCalc1)
		soundNote2.set_pitch_scale(220 / pitchCalc2)
		if(("sound:position" in paramsToSet) || ("sound:volume" in paramsToSet)):
			soundNote1.play()
			soundNote2.play()
		soundTimeout = soundDelay
	else:
		soundTimeout -= delta
	timeLabel.text = " %0.1f s" % runTime

func _on_mouse_shape_entered(shape_idx):
	if patch.get_child(shape_idx) is CollisionShape2D:
		var patchShape: CollisionShape2D = patch.get_child(shape_idx)
		if(patchShape.name.begins_with("Patch_")):
			patchCover.visible = true
			patchCover.transform.origin = patchShape.transform.origin
			var soundNote1 = soundListener.get_node("Note_A220_a1")
			var soundNote2 = soundListener.get_node("Note_A220_a2")
			var skillSndPos = root.skillLevels["sound:position"] if "sound:position" in root.skillLevels else 0
			soundNote1.position = patchCover.position * 1.0 / ((skillSndPos * 1.5) + 1.0)
			soundNote2.position = patchCover.position * 1.0 / ((skillSndPos * 1.5) + 1.0)
			var pitchCalc1 = 220 * 2.0 ** (-soundNote1.position.x / 320.0)
			var pitchCalc2 = 220 * (2.0/3.0) * 2.0 ** (soundNote2.position.y / 320.0)
			soundNote1.set_pitch_scale(220 / pitchCalc1)
			soundNote2.set_pitch_scale(220 / pitchCalc2)
			if(("sound:position" in paramsToSet) || ("sound:volume" in paramsToSet)):
				soundNote1.play()
				soundNote2.play()

func _on_mouse_shape_exited(shape_idx):
	if patch.get_child(shape_idx) is CollisionShape2D:
		var patchShape: CollisionShape2D = patch.get_child(shape_idx)
		if(patchShape.name.begins_with("Patch_")):
			patchCover.frame = 0
			patchSelectTimeout = 0
			patchCover.visible = false

func _on_input_event(_viewport, event, shape_idx):
	var patchShape: CollisionShape2D = patch.get_child(shape_idx)
	if event is InputEventMouseButton:
		if(event.pressed):
			if(patchShape.name == modifiedPatch):
				successCount += 1
				points += minScoreLevel * (root.PARAM_NAMES.size() - differenceCount + 1)
				scoreLabel.text = "Score\n" + str(points)
				if(successCount >= 2):
					differenceCount -= 1
					if(differenceCount <= 0):
						## Increment all levels
						differenceCount = root.PARAM_NAMES.size()
						var paramsToUpdate: Array = root.PARAM_NAMES.duplicate()
						for setParam in paramsToUpdate:
							if(setParam in root.skillLevels):
								root.skillLevels[setParam] += 1
							else:
								root.skillLevels[setParam] = 1
						var save_file = FileAccess.open("user://skills.save", FileAccess.WRITE)
						save_file.store_line(JSON.stringify(root.skillLevels))
					successCount = 0
				failCount = 0
				patchCover.frame = 2
				blurbText.visible = false
			else:
				patchCover.frame = 1
				failCount += 1
				successCount = 0
				if(failCount >= 3):
					finishTrainer()
				else:
					blurbText.visible = false
			resetShapes()
			runTime = 0.0
		else:
			patchSelectTimeout = TIMEOUT_PATCH_SELECT
