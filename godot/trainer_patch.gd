extends Area2D

@onready var patchCover: AnimatedSprite2D = $Cover
@onready var surroundFail: Sprite2D = self.get_parent().get_node("SurroundFail")
@onready var patch: Area2D = self
@onready var timeLabel: Label = $TimeLabel
@onready var skillLabel: Label = $SkillLabel
@onready var gameNode: Node2D = get_parent()
@onready var root: Node = self.get_tree().root.get_node("Gabohren")
@onready var blurbText: RichTextLabel = self.get_parent().get_node("BlurbText")
@onready var soundListener: AudioListener2D = self.get_parent().get_node("Listener_Primary")
@onready var soundNote: AudioStreamPlayer2D

const PATCH_NAMES = ["Patch_N","Patch_NE","Patch_E","Patch_SE",
					 "Patch_S","Patch_SW","Patch_W","Patch_NW"]
const TIME_MAX = 20.0
const TIMEOUT_PATCH_ALL = 0.2
const TIMEOUT_PATCH_SELECT = 0.2
const SOUND_DELAY_START = 5.0
const MOD_TYPE_LINEAR = 1
const MOD_TYPE_EXPONENTIAL = 2
const MOD_TYPE_SOUND = 3

var cParam:Dictionary = {}
var paramName: String
var modifiedPatch = PATCH_NAMES[0]
var skillLevel = 0
var failCount = 0
var lastGoodSkillLevel = 0
var nextParams:Array = Array()

var runTime = 0.0
var patchAllTimeout = 0.0
var patchSelectTimeout = 0.0
var soundTimeout = 0.0
var soundDelay = SOUND_DELAY_START

func finishTrainer():
	root.skillLevels[paramName] = skillLevel
	var save_file = FileAccess.open("user://skills.save", FileAccess.WRITE)
	save_file.store_line(JSON.stringify(root.skillLevels))
	if(root.selectedTrainer != ""):
		paramName = root.selectedTrainer
		root.goto_scene("res://gabohren.tscn")
	else:
		pickNewTrainer()
		resetShapes()
	#get_tree().quit()

func pickNewTrainer():
	patchCover.frame = 0
	patchCover.visible = false
	surroundFail.visible = false
	blurbText.visible = true
	skillLevel = 0
	failCount = 0
	skillLabel.text = "Skill level: %d" % skillLevel
	timeLabel.text = " %0.1f s" % runTime
	if(nextParams.is_empty()):
		nextParams = root.PARAM_NAMES.duplicate()
		nextParams.shuffle()
	if(root.selectedTrainer != ""):
		paramName = root.selectedTrainer
	else:
		paramName = nextParams.pop_back()
	cParam = root.PARAM_DATA[paramName]
	blurbText.text = "[center][font_size=50]" + cParam["name"] + \
			"[/font_size][/center]\n\n" + cParam["blurb"]

# Called when the node enters the scene tree for the first time.
func _ready():
	root.activeScene = "Trainer"
	randomize()
	pickNewTrainer()
	resetShapes()
	soundListener.make_current()
	blurbText.visible = true

func resetShapes():
	modifiedPatch = PATCH_NAMES[randi() % PATCH_NAMES.size()]
	soundTimeout = 0
	var diffDir = (randi() % 2 - 0.5) * 2.0
	var epsilon = 0.001
	var setParam = paramName
	if(paramName.find(":") > -1):
		setParam = paramName.substr(0, paramName.find(":"))
	var paramModDiff = (cParam["modMax"] - cParam["modMin"]) / (2.0 * 1.2 ** skillLevel)
	var paramMidLow = cParam["modMin"] + paramModDiff
	var paramMidHigh = cParam["modMax"] - paramModDiff
	var paramModMid = paramMidLow
	if(paramMidHigh - paramMidLow > epsilon):
		paramModMid = randf() * (paramMidHigh - paramMidLow) + paramMidLow
	for iChild in self.get_children():
		if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
			var icImage: Sprite2D = iChild.get_node("PatchImage")
			var icMaterial := icImage.material as ShaderMaterial
			## Reset everything to default values
			icMaterial.set_shader_parameter("speed_phase", 0.0)
			icMaterial.set_shader_parameter("speed_rotation", 0.0)
			icMaterial.set_shader_parameter("offset_phase", 0.0)
			icMaterial.set_shader_parameter("offset_rotation", 0.0)
			icMaterial.set_shader_parameter("frequency", 2.0)
			icMaterial.set_shader_parameter("scale", 1.0)
			icMaterial.set_shader_parameter("contrast", 1.0)
			if(paramName.contains(":RG")): # special case for red/green
				icMaterial.set_shader_parameter("colour1", Color(1.0,0.0,0.0)) # red
				icMaterial.set_shader_parameter("colour2", Color(0.0,1.0,0.0)) # green
			elif(paramName.contains(":BY")): # special case for blue/yellow
				icMaterial.set_shader_parameter("colour1", Color(0.0,0.0,1.0)) # blue
				icMaterial.set_shader_parameter("colour2", Color(1.0,1.0,0.0)) # yellow
			else:
				icMaterial.set_shader_parameter("colour1", Color(1.0,1.0,1.0)) # white
				icMaterial.set_shader_parameter("colour2", Color(0.0,0.0,0.0)) # black
			if(iChild.name == modifiedPatch):
				if(cParam["modType"] == root.MOD_TYPE_SOUND):
					pass
				if(cParam["modType"] == root.MOD_TYPE_EXPONENTIAL):
					icMaterial.set_shader_parameter(setParam,
						cParam["base"] * (2.0 ** (paramModMid + paramModDiff * diffDir)) )
				elif(cParam["modType"] == root.MOD_TYPE_LINEAR):
					icMaterial.set_shader_parameter(setParam,
						paramModMid + paramModDiff * diffDir)
			else:
				if(cParam["modType"] == root.MOD_TYPE_EXPONENTIAL):
					icMaterial.set_shader_parameter(setParam, cParam["base"] * (2.0 ** paramModMid) )
				elif(cParam["modType"] == root.MOD_TYPE_LINEAR):
					icMaterial.set_shader_parameter(setParam, paramModMid )

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	runTime += delta
	var ramp = sqrt(minf(runTime, TIME_MAX) / TIME_MAX)
	var rtScale = ramp * 1.9 + 0.1
	if(paramName.begins_with("sound")):
		rtScale = 2
	for iChild in self.get_children():
		if(iChild is CollisionShape2D and iChild.name.begins_with("Patch_")):
			var icImage: Sprite2D = iChild.get_node("PatchImage")
			icImage.scale = Vector2(rtScale, rtScale)
	if(runTime >= TIME_MAX):
		surroundFail.visible = true
		surroundFail.frame = 1
		patchAllTimeout = TIMEOUT_PATCH_ALL
		skillLevel = 0 if skillLevel <= 1 else (skillLevel - 1)
		failCount += 1
		skillLabel.text = "Skill level: %d" % skillLevel
		runTime = 0.0
		blurbText.visible = true
		if(failCount >= 2):
			finishTrainer()
	if(patchAllTimeout > 0):
		patchAllTimeout -= delta
	else:
		surroundFail.visible = false
	if(patchSelectTimeout > 0):
		patchSelectTimeout -= delta
		if(patchSelectTimeout <= 0):
			patchCover.visible = false
			patchSelectTimeout = 0
	if(paramName.begins_with("sound") and (soundTimeout <= 0)):
		var patchObject = patch.get_node(modifiedPatch)
		var soundNote1 = soundListener.get_node("Note_A220_b1")
		var soundNote2 = soundListener.get_node("Note_A220_b2")
		if(paramName == "sound:volume"):
			soundNote1.position = patchObject.position
			soundNote2.position = patchObject.position
		else:
			soundNote1.position = patchObject.position * 1.0 / ((skillLevel * 1.5) + 1.0)
			soundNote2.position = patchObject.position * 1.0 / ((skillLevel * 1.5) + 1.0)
		if(paramName == "sound:volume"):
			soundNote1.attenuation = 5.0 * 2.0 ** (skillLevel / 10.0)
			soundNote2.attenuation = 5.0 * 2.0 ** (skillLevel / 10.0)
			soundNote1.volume_db = -skillLevel * (1.0 - (runTime / TIME_MAX))
		var pitchCalc1 = 220 * 2.0 ** (-soundNote1.position.x / 320.0)
		var pitchCalc2 = 220 * (2.0/3.0) * 2.0 ** (soundNote2.position.y / 320.0)
		soundNote1.set_pitch_scale(220 / pitchCalc1)
		soundNote2.set_pitch_scale(220 / pitchCalc2)
		soundNote1.play()
		soundNote2.play()
		soundTimeout = soundDelay
	else:
		soundTimeout -= delta
	timeLabel.text = " %0.1f s" % runTime

func _on_mouse_shape_entered(shape_idx):
	if patch.get_child(shape_idx) is CollisionShape2D:
		var patchShape: CollisionShape2D = patch.get_child(shape_idx)
		if(patchShape.name.begins_with("Patch_")):
			patchCover.visible = true
			patchCover.transform.origin = patchShape.transform.origin
			if(paramName.begins_with("sound")):
				var soundNote1 = soundListener.get_node("Note_A220_a1")
				var soundNote2 = soundListener.get_node("Note_A220_a2")
				if(paramName == "sound:volume"):
					soundNote1.position = patchCover.position
					soundNote2.position = patchCover.position
				else:
					soundNote1.position = patchCover.position * 1.0 / ((skillLevel * 1.5) + 1.0)
					soundNote2.position = patchCover.position * 1.0 / ((skillLevel * 1.5) + 1.0)
				var pitchCalc1 = 220 * 2.0 ** (-soundNote1.position.x / 320.0)
				var pitchCalc2 = 220 * (2.0/3.0) * 2.0 ** (soundNote2.position.y / 320.0)
				soundNote1.set_pitch_scale(220 / pitchCalc1)
				soundNote1.play()
				soundNote2.set_pitch_scale(220 / pitchCalc2)
				soundNote2.play()

func _on_mouse_shape_exited(shape_idx):
	if patch.get_child(shape_idx) is CollisionShape2D:
		var patchShape: CollisionShape2D = patch.get_child(shape_idx)
		if(patchShape.name.begins_with("Patch_")):
			patchCover.frame = 0
			patchSelectTimeout = 0
			patchCover.visible = false

func _on_input_event(_viewport, event, shape_idx):
	var patchShape: CollisionShape2D = patch.get_child(shape_idx)
	if event is InputEventMouseButton:
		if(event.pressed):
			if(patchShape.name == modifiedPatch):
				lastGoodSkillLevel = skillLevel
				patchCover.frame = 2
				skillLevel += 1
				failCount = 0
				blurbText.visible = false
			else:
				patchCover.frame = 1
				skillLevel = 0 if skillLevel <= 1 else (skillLevel - 1)
				failCount += 1
				if(failCount >= 2):
					finishTrainer()
				else:
					blurbText.visible = false
			skillLabel.text = "Skill level: %d" % skillLevel
			resetShapes()
			runTime = 0.0
		else:
			patchSelectTimeout = TIMEOUT_PATCH_SELECT
